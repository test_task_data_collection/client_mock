package client_mock

import (
	"context"
	"fmt"

	"github.com/valyala/fasthttp"
	"go.uber.org/zap"

	"gitlab.com/test_task_data_collection/client_mock/internal/app/client_mock/http/client"
	"gitlab.com/test_task_data_collection/client_mock/internal/config"
)

type Application struct {
	Config     *config.Config
	Logger     *zap.Logger
	HttpClient *fasthttp.Client
}

func (app *Application) Run(ctx context.Context) {
	cli := client.Client{
		Logger:     app.Logger,
		Config:     app.Config,
		HttpClient: app.HttpClient,
	}

	if err := cli.Start(ctx); err != nil {
		app.Logger.Error("Client is down", zap.String("type", "http"), zap.Error(err))
	}
}

func (app *Application) Shutdown() error {
	app.Logger.Info("Shutdown logger")
	if err := app.Logger.Sync(); err != nil {
		return fmt.Errorf("shutdown logger: %w", err)
	}

	return nil
}
