package client

import (
	"context"
	"fmt"
	"sync/atomic"
	"time"

	"github.com/valyala/fasthttp"
	"go.uber.org/zap"

	"gitlab.com/test_task_data_collection/client_mock/internal/config"
)

type Client struct {
	Logger     *zap.Logger
	Config     *config.Config
	HttpClient *fasthttp.Client
}

func (c *Client) Start(ctx context.Context) error {
	c.Logger.Info("Client is running")

	//250 rps
	requestTicker := time.NewTicker(4 * time.Millisecond)
	endTicker := time.NewTicker(300 * time.Second)
	start := time.Now()
	var counter atomic.Int64

	for {
		select {
		case <-requestTicker.C:
			go func() {
				c.SendAnalyticEvents()
				counter.Add(1)
			}()
		case <-endTicker.C:
			elapsed := time.Since(start)
			rps := float64(counter.Load()) / elapsed.Seconds()
			fmt.Println(rps)
			return nil
		case <-ctx.Done():
			return nil
		}
	}
}
