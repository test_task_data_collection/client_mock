package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"time"

	"github.com/valyala/fasthttp"
	"go.uber.org/zap"

	"gitlab.com/test_task_data_collection/client_mock/internal/config"
	"gitlab.com/test_task_data_collection/client_mock/internal/models"
)

const (
	contentTypeApplicationJson = "application/json"

	uriPathAdEvents = "/ad_events"
)

func (c *Client) SendAnalyticEvents() {
	rand.Seed(time.Now().UnixNano())

	body, err := marshalAdEvents(prepareAdEventsBatch())
	if err != nil {
		c.Logger.Error("marshal ad events", zap.Error(err))
		return
	}

	req, resp := fasthttp.AcquireRequest(), fasthttp.AcquireResponse()
	defer func() {
		fasthttp.ReleaseRequest(req)
		fasthttp.ReleaseResponse(resp)
	}()

	preparePostRequest(req, c.Config.HTTPClient, body)

	if err = c.HttpClient.Do(req, resp); err != nil {
		c.Logger.Error("error while sending events", zap.Error(err))
		return
	}

	if len(resp.Body()) == 0 {
		c.Logger.Error("empty body in response")
		return
	}
}

func preparePostRequest(req *fasthttp.Request, httpClientCfg config.HTTPClient, body []byte) {
	req.SetBody(body)

	req.Header.Set("Connection", "keep-alive")
	req.Header.SetMethod(http.MethodPost)
	req.Header.SetContentType(contentTypeApplicationJson)
	req.Header.SetContentLength(len(req.Body()))

	req.SetRequestURI(fmt.Sprint(httpClientCfg.ServerURL + uriPathAdEvents))
}

func marshalAdEvents(adEvents []models.AdEvent) ([]byte, error) {
	var buf bytes.Buffer
	enc := json.NewEncoder(&buf)
	for _, event := range adEvents {
		if err := enc.Encode(event); err != nil {
			return nil, fmt.Errorf("encode ad event: %w", err)
		}

		// enc.Encode already adds new line under the hood
		//if _, err := buf.WriteString("\n"); err != nil {
		//	return nil, fmt.Errorf("write new line: %w", err)
		//}
	}

	return buf.Bytes(), nil
}

func prepareAdEventsBatch() []models.AdEvent {
	// use 61 to get 30 avg
	eventCount := rand.Intn(61)

	adEvents := make([]models.AdEvent, 0, eventCount)
	for i := 0; i < eventCount; i++ {
		adEvents = append(adEvents, models.AdEvent{
			ClientTime: time.Now(),
			DeviceID:   deviceIDsMockList[rand.Intn(len(deviceIDsMockList))],
			DeviceOS:   deviceOSMockList[rand.Intn(len(deviceOSMockList))],
			Session:    sessionMockList[rand.Intn(len(sessionMockList))],
			Event:      eventMockList[rand.Intn(len(eventMockList))],
			ParamStr:   paramStrMockList[rand.Intn(len(paramStrMockList))],
			Sequence:   rand.Intn(40),
			ParamInt:   rand.Intn(40),
		})
	}
	return adEvents
}

var (
	deviceIDsMockList = []string{"2bdd19ce-a7dc-11ed-afa1-0242ac120002", "63d0d12c-a7dc-11ed-afa1-0242ac120002",
		"66feca2a-a7dc-11ed-afa1-0242ac120002", "6cc0cfc6-a7dc-11ed-afa1-0242ac120002",
		"77295816-a7dc-11ed-afa1-0242ac120002", "7e3ecea6-a7dc-11ed-afa1-0242ac120002",
		"83d40250-a7dc-11ed-afa1-0242ac120002", "87738bc4-a7dc-11ed-afa1-0242ac120002",
		"92785b08-a7dc-11ed-afa1-0242ac120002", "97c06678-a7dc-11ed-afa1-0242ac120002"}
	deviceOSMockList = []string{"OS 13.5.1", "OS 13.5.2", "OS 13.5.3", "OS 13.5.4", "OS 13.5.5", "Android 11.22",
		"Android 11.23", "Android 11.24", "Android 11.25", "Android 11.26", "Android 11.27"}
	sessionMockList = []string{"dfgjfgjdfgj", "jhkhjkhjk", "fgmjhmjhmfh", "ghmfgmghmg", "dghmdghmdgm", "dghmghmgm22",
		"asdvasdvasvd", "fgndfgndfgn", "sdavasvsdv", "dfbsdbf"}
	eventMockList = []string{"app_start1", "app_start2", "app_start3", "app_start4", "app_start5", "app_finish1",
		"app_start2", "app_start3", "app_start4", "app_start5"}
	paramStrMockList = []string{"some text 1", "some text 2", "some text 3", "some text 4", "some text 5", "some text 6",
		"some text 7", "some text 8", "some text 9", "some text 10"}
)
