package models

import "time"

type AdEvent struct {
	ClientTime time.Time `json:"client_time"`
	DeviceID   string    `json:"device_id"`
	DeviceOS   string    `json:"device_os"`
	Session    string    `json:"session"`
	Sequence   int       `json:"sequence"`
	Event      string    `json:"event"`
	ParamInt   int       `json:"param_int"`
	ParamStr   string    `json:"param_str"`
}
