package logger

import (
	"fmt"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func New(name, env, level string, opts ...zap.Option) (*zap.Logger, error) {
	var config zap.Config
	switch env {
	default:
		config = zap.NewDevelopmentConfig()
	}
	config.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder

	err := config.Level.UnmarshalText([]byte(level))
	if err != nil || len(level) == 0 {
		config.Level.SetLevel(zap.DebugLevel)
	}

	logger, err := config.Build(opts...)
	if err != nil {
		return nil, fmt.Errorf("build logger: %w", err)
	}

	logger = logger.With(
		zap.String("name", name),
		zap.String("env", env),
	)

	return logger, nil
}
