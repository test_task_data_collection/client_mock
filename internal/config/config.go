package config

import (
	"fmt"
	"os"

	"github.com/spf13/viper"
)

const cfgFilePathTemplate = "%s/configs/%s.yml"

type Config struct {
	HTTPClient HTTPClient `mapstructure:"http_client"`
}

type HTTPClient struct {
	ServerURL string `mapstructure:"server_url"`
}

func NewConfig(configFilePath string) (*Config, error) {
	config, err := loadConfig(configFilePath)
	if err != nil {
		return nil, fmt.Errorf("loadConfig: %w", err)
	}

	return config, nil
}

func loadConfig(configFilePath string) (*Config, error) {
	viper.SetConfigFile(configFilePath)
	if err := viper.ReadInConfig(); err != nil {
		return nil, fmt.Errorf("readInConfig: %w", err)
	}

	var appConfig Config
	if err := viper.Unmarshal(&appConfig); err != nil {
		return nil, fmt.Errorf("unmarshal config: %w", err)
	}

	return &appConfig, nil
}

func PrepareCfgFilePath(env, currentPath, appPathEnvName string) string {
	appPath := currentPath
	if len(os.Getenv(appPathEnvName)) > 0 {
		appPath = os.Getenv(appPathEnvName)
	}

	return fmt.Sprintf(cfgFilePathTemplate, appPath, env)
}
