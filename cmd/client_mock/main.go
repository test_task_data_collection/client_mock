package main

import (
	"context"
	"crypto/tls"
	"flag"
	"log"
	"os/signal"
	"syscall"
	"time"

	"github.com/valyala/fasthttp"

	"gitlab.com/test_task_data_collection/client_mock/internal/app/client_mock"
	"gitlab.com/test_task_data_collection/client_mock/internal/config"
	ll "gitlab.com/test_task_data_collection/client_mock/internal/logger"
)

const (
	serviceName = "client_mock"

	appPathEnvName = "APP_PATH"

	currentPath = "."

	envFlagName         = "e"
	envFlagDefault      = "local"
	logLevelFlagName    = "ll"
	logLevelFlagDefault = "local"
)

func main() {
	// Create context that listens for the interrupt signal from the OS.
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	var env, logLevel string
	flag.StringVar(&env, envFlagName, envFlagDefault, "environment")
	flag.StringVar(&logLevel, logLevelFlagName, logLevelFlagDefault, "logging level")
	flag.Parse()

	cfg, err := config.NewConfig(config.PrepareCfgFilePath(env, currentPath, appPathEnvName))
	if err != nil {
		log.Fatal("error while reading config", err.Error())
	}

	logger, err := ll.New(serviceName, env, logLevel)
	if err != nil {
		log.Println("error while initing logger", err.Error())
	}

	app := &client_mock.Application{
		Logger: logger,
		Config: cfg,
		HttpClient: &fasthttp.Client{
			Name: serviceName,
			TLSConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
			MaxConnsPerHost: 10000,
		},
	}

	go app.Run(ctx)

	// Listen for the interrupt signal.
	<-ctx.Done()

	// Restore default behavior on the interrupt signal and notify user of shutdown.
	stop()
	log.Println("shutting down gracefully")

	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	_, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err = app.Shutdown(); err != nil {
		log.Fatal("Server forced to shutdown: ", err)
	}

	log.Println("Server exiting")
}
